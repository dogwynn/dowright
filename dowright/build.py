import logging
import pprint
from typing import Union, List
import ipaddress
import copy
import time
import math
import bisect
import re
from collections import OrderedDict
from functools import lru_cache
from itertools import chain

import jinja2
from digitalocean import Manager, Droplet, Volume
from memoized_property import memoized_property
from gems import composite
import paramiko
from paramiko import SSHClient
from tokenmanager import get_tokens

from . import yaml
from .digitalocean_fix import VolumeWithSnapshot

memoize = lru_cache(None)

log = logging.getLogger('build')
log.addHandler(logging.NullHandler())

standard_re = re.compile(r'^s-(?P<cpu>\d+)vcpu-(?P<mem>\d+)gb$')
ded_re = re.compile(r'^c-(?P<cpu>\d+)$')

def image_sizes(sizes):
    images = []
    for s in sizes:
        smatch, dmatch = standard_re.match(s.slug), ded_re.match(s.slug)
        if smatch or dmatch:
            images.append((s.vcpus, s.memory // 1024, bool(dmatch)))
    return images

def get_slug(sizes, cpus, mem, dedicated=False):
    images = [i for i in image_sizes() if i[2]==dedicated]

    max_cpus_s = max([c for c,_,d in images if not d])
    max_cpus_d = max([c for c,_,d in images if d])
    max_mem_s = max([m for _,m,d in images if not d])
    max_mem_d = max([m for _,m,d in images if d])

    cpus = min(cpus, max_cpus_d) if dedicated else min(cpus, max_cpus_s)
    mem = min(mem, max_mem_d) if dedicated else min(mem, max_mem_s)
    
    valid = [i for i in images if i[1] >= mem and i[0] >= cpus]
    r_cpus, r_mem = valid[0][:2]
    if dedicated:
        slug = 'c-{}'.format(r_cpus)
    else:
        slug = 's-{}vcpu-{}gb'.format(r_cpus, r_mem)
    return slug

class DOWrightBuildError(Exception):
    pass


def get_token(dotoken_type:str) -> str:
    tokens = get_tokens()
    return tokens.digitalocean[dotoken_type]


def find_record(r, domain):
    test = (r['type'], r['name'], r['data'])
    for record in domain.get_records():
        if test == (record.type, record.name, record.data):
            return record


def is_ip_address(address):
    try:
        ipaddress.ip_address(address)
    except ValueError:
        return False
    return True


_ssh_clients = {}
def droplet_ssh(droplet: Droplet) -> SSHClient:
    if droplet.name in _ssh_clients:
        return _ssh_clients[droplet.name]
    ssh = paramiko.SSHClient()
    ssh.load_system_host_keys()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(droplet.ip_address, username='root')
    _ssh_clients[droplet.name] = ssh
    return ssh


def close_ssh_clients():
    for client in _ssh_clients.values():
        client.close()


def droplet_file_exists(droplet:Droplet, path:str):
    # with droplet_ssh(droplet) as ssh:
    #     _,stdout,_ = ssh.exec_command(
    #         'cat {} &> /dev/null && echo OK'.format(path)
    #     )
    #     exists = bool(stdout.read())
    ssh = droplet_ssh(droplet)
    _,stdout,_ = ssh.exec_command(
        'cat {} &> /dev/null && echo OK'.format(path)
    )
    exists = bool(stdout.read())
    return exists


cc_udata_template = lambda: jinja2.Template('''\
#cloud-config
runcmd:
{{ commands }}
''')
def get_cloud_config_user_data(commands:list=None) -> str:
    template = cc_udata_template()
    commands = commands or []

    commands.append('touch /.cloud-config-done')
    log.info(commands)

    return template.render(
        commands=yaml.dump(commands),
    )


class Builder:
    def __init__(self, spec:dict, dry_run:bool=False):
        self.spec = spec
        self.dry_run = dry_run
        self.prebuild_functions = []
        self.postbuild_functions = []

    @memoized_property
    def token(self) -> str:
        return get_token(self.spec['token'])

    @memoized_property
    def manager(self) -> Manager:
        return Manager(token=self.token)

    @property
    def droplets(self) -> list:
        prefix = self.spec['prefix']
        return [d for d in self.manager.get_all_droplets()
                if d.name.startswith(prefix)]

    @property
    def floating_ips(self) -> list:
        ips = self.manager.get_all_floating_ips()
        return ips if ips else []

    @property
    def domains(self) -> list:
        return self.manager.get_all_domains()

    @property
    def volumes(self) -> list:
        return self.manager.get_all_volumes()

    @property
    def snapshots(self) -> list:
        return chain(*[d.get_snapshots() for d in self.droplets])

    def get_slug(self, cpus, mem, dedicated=False):
        return get_slug(self.manager.get_all_sizes(), cpus, mem, dedicated)

    @property
    def droplet_configurations(self) -> list:
        configs = OrderedDict()
        do_dlut = {d.name: d for d in self.droplets}
        domains = self.domains
        floating_ips = self.floating_ips
        for dgroup, droplets in self.spec['droplets'].items():
            for d in droplets:
                kwargs = self.droplet_kwargs(d, [dgroup])
                do_droplet = do_dlut[kwargs['name']]
                fips = [ip.ip for ip in floating_ips
                        if (ip.droplet and ip.droplet['name']==kwargs['name'])]
                allips = set([do_droplet.ip_address] + fips)
                hostnames = []
                for domain in domains:
                    for r in domain.get_records():
                        if r.data in allips:
                            hostnames.append('.'.join([r.name,domain.name]))
                config = {
                    'spec': kwargs,
                    'dod': do_droplet,
                    'fips': fips,
                    'groups': [dgroup],
                    'hosts': hostnames,
                }
                configs[kwargs['name']] = config
        return configs.values()

    def droplet_name(self, name:str) -> str:
        prefix = self.spec['prefix']
        return '{}-{}'.format(prefix, name)

    def prep_domain_records(self, records:list, droplets:list)->list:
        records = records[:]
        for i,rec in enumerate(records):
            rec = rec.copy()
            if not is_ip_address(rec['data']):
                dname = self.droplet_name(rec['data'])
                droplet = [d for d in droplets if d.name == dname][0]
                rec['data'] = droplet.ip_address
            records[i] = rec
        return records

    def prep_droplet(self, droplet:dict) -> dict:
        new = copy.deepcopy(self.spec.get('defaults',{}))
        new.update(copy.deepcopy(droplet))

        new['name'] = self.droplet_name(droplet['name'])
        new['token'] = self.token

        config_commands = new.pop('cloud_config_commands',[])
        ss_volume_commands = self.extract_volume_info(new)
        config_commands = (
            ss_volume_commands +
            config_commands
        )
        
        if config_commands:
            new['user_data'] = get_cloud_config_user_data(
                config_commands
            )

        if 'snapshot_image' in new:
            ss_name = new.pop('snapshot_image')
            ss_id = [s for s in self.manager.get_all_snapshots()
                     if s.name == ss_name][0].id
            new['image'] = ss_id

        return new

    def extract_volume_info(self, droplet):
        config_commands = []
        commands = ['mkdir -p {mount}',
                    'mount -o discard,defaults'
                    ' /dev/disk/by-id/scsi-0DO_Volume_{name} {mount}',
                    'echo /dev/disk/by-id/scsi-0DO_Volume_{name} {mount}'
                    ' ext4 defaults,nofail,discard 0 0 |'
                    ' sudo tee -a /etc/fstab']

        volumes = droplet.pop('volume_info', [])
        names = {v['name'] for v in volumes}
        v_names = {v.name for v in self.volumes}
        new_volumes = [v for v in volumes
                       if v['name'] in (names - v_names)]

        all_snapshots = self.manager.get_all_snapshots()
        ss_ids = {s.name: s.id for s in all_snapshots
                  if s.resource_type=='volume'}
        region = droplet['region']

        for v in new_volumes:
            kwargs = {
                'name': v['name'], 'size': v['size'],
            }
            if 'snapshot_name' in v:
                kwargs['snapshot_id'] = ss_ids[v['snapshot_name']]
            else:
                kwargs['region'] = region
            self.build_volume(**kwargs)

        droplet.setdefault('volumes',[]).extend(
            v.id for v in self.volumes if v.name in names
        )

        for v in volumes:
            config_commands.extend([c.format(**v) for c in commands])

        return config_commands
            
    def build_volume(self, name, size, snapshot_id=None, region=None):
        log.info('Building volume')
        kwargs = {
            'size_gigabytes': size,
            'name': name,
            'token': self.token,
        }
        if snapshot_id:
            kwargs['snapshot_id'] = snapshot_id
        elif region:
            kwargs['region'] = region
        else:
            log.error('Must provide either snapshot id xor region')

            class VolumeBuildError(Exception):
                pass
            raise VolumeBuildError(pprint.pformat(kwargs))

        volume = VolumeWithSnapshot(**kwargs)
        if not self.dry_run:
            log.info('  .. building volume: %s', pprint.pformat(kwargs))
            new_v = volume.create_from_snapshot(**kwargs)
            log.info('  .. built: %s', new_v)
            return new_v
        else:
            log.info('  .. DRY RUN .. did not build.', name)

    def droplet_kwargs(self, droplet:dict, tags:List[str]=None) -> dict:
        kwargs = self.prep_droplet(droplet)
        if tags:
            if 'tags' in kwargs:
                kwargs['tags'].extend(tags)
            else:
                kwargs['tags'] = tags
        return kwargs

    def build_droplets(self) -> None:
        log.info('Building droplets:')
        existing = set(d.name for d in self.droplets)
        prefix = self.spec['prefix']
                
        for droplet_tag, droplets in self.spec['droplets'].items():
            tags = [droplet_tag, prefix, ]
            kwargs_list = filter(
                lambda kw:kw['name'] not in existing,
                [self.droplet_kwargs(d, tags) for d in droplets]
            )
            # for kwargs in sorted(kwargs_list, key=lambda d:d['name']):
            for kwargs in kwargs_list:
                log.info('  .. building droplet: %s', kwargs['name'])
                log.info('%s', pprint.pformat(kwargs))
                droplet = Droplet(**kwargs)
                if not self.dry_run:
                    droplet.create()
                else:
                    log.info('  .. DRY RUN .. did not build.')
                
        if not self.dry_run:
            droplets = self.droplets
            dlut = {d.name:d for d in droplets}
            new_droplets = [
                dlut[n] for n in set(d.name for d in droplets) - existing
            ]
            log.info('New droplets created:')
            for d in new_droplets:
                log.info('  .. name: %s ip: %s',d.name, d.ip_address)
        else:
            log.info('DRY RUN .. no droplets created.')

    def destroy_droplets(self)->None:
        droplets = self.droplets
        floating_ips = self.floating_ips
        domains = self.domains
        all_volumes = self.volumes
        volume_ids = {vid for d in droplets for vid in d.volume_ids}
        volumes = [v for v in all_volumes if v.id in volume_ids]
        
        if droplets:
            log.info('Destroying all (%s) droplets...', len(droplets))

            for ip in floating_ips:
                if (ip.droplet and ip.droplet['name'] in {d.name
                                                          for d in droplets}):
                    log.info('  .. unassigning IP (%s) for droplet (%s)',
                             ip.ip, ip.droplet['name'])
                    if not self.dry_run:
                        ip.unassign()
                    else:
                        log.info('  .. DRY RUN .. did not unassign.')

            for domain_name, records in self.spec['domains'].items():
                domain = [d for d in domains if d.name == domain_name][0]
                records = self.prep_domain_records(records, droplets)
                for r in records:
                    record = find_record(r, domain)
                    if record:
                        log.info('  .. removing domain record: %s %s %s',
                                 record.type, record.name, record.data)
                        if not self.dry_run:
                            record.destroy()
                        else:
                            log.info('  .. DRY RUN .. did not remove.')

            log.info('  .. destroying droplets')
            for d in droplets:
                log.info('    .. {}'.format(d.name))
                if not self.dry_run:
                    d.destroy()
                else:
                    log.info('    .. DRY RUN .. did not destroy.')

            while self.droplets:
                log.info('  .. waiting for droplets to be destroyed')
                time.sleep(1)

            volumes_to_destroy = set()
            for g in self.spec['droplets']:
                for d in self.spec['droplets'][g]:
                    log.info(d)
                    if 'volume_info' in d:
                        for sv in d['volume_info']:
                            if not sv.get('persist'):
                                volumes_to_destroy.add(sv['name'])
            log.info(volumes_to_destroy)
            for v in volumes:
                log.info(v)
                if v.name in volumes_to_destroy:
                    if not self.dry_run:
                        log.info('  .. destroying volume: %s', v.name)
                        v.destroy()
                    else:
                        log.info('  .. DRY RUN did not destroy.')

        else:
            log.info('No droplets to destroy.')

    def configure_floating_ips(self) -> None:
        droplets = self.droplets
        floating_ips = self.floating_ips

        if 'floating_ips' in self.spec:
            log.info('Configuring floating IPs:')
            for ip,name in self.spec['floating_ips'].items():
                fip = [i for i in floating_ips if i.ip == ip]
                if not fip:
                    raise DOWrightBuildError(
                        'No floating IP with ip: {}'.format(ip)
                    )
                fip = fip[0]
                droplet_name = self.droplet_name(name)
                droplets = [d for d in droplets if d.name==droplet_name]
                if not droplets:
                    raise DOWrightBuildError(
                        'No droplet for this spec with'
                        ' name: {} ({})'.format(name, droplet_name)
                    )
                droplet = droplets[0]
                if ((not fip.droplet) or (fip.droplet and
                                          fip.droplet['id'] != droplet.id)):
                    log.info('  assigning %s to %s', fip.ip, droplet.name)
                    if not self.dry_run:
                        fip.assign(droplet.id)
                    else:
                        log.info('  .. DRY RUN .. no assignment.')
        else:
            log.info('No floating IPs to configure.')

    def configure_domains(self) -> None:
        if 'domains' in self.spec:
            log.info('Configuring domains:')

            droplets = self.droplets
            domains = self.domains
            found = False
            for domain_name, records in self.spec['domains'].items():
                domain = [d for d in domains if d.name == domain_name][0]
                records = self.prep_domain_records(records, droplets)
                for r in records:
                    if not find_record(r, domain):
                        found = True
                        kw = {'type':r['type'], 'name':r['name'],
                              'data':r['data']}
                        log.info('  configuring |type: %s|  |name: %s|'
                                 '  |data: %s|',
                                 kw['type'], kw['name'], kw['data'])
                        if not self.dry_run:
                            domain.create_new_domain_record(**kw)
                        else:
                            log.info('  .. DRY RUN .. not configured.')
            if not found:
                log.info('No new domains to be configured.')
        else:
            log.info('No domains to configure.')

    def droplets_still_building(self):
        return [d for d in self.droplets if d.locked]

    def droplets_still_configuring(self):
        ssh_errors = 0
        done = False
        still_configuring = None
        ssh_error = None
        while ssh_errors < 5 and not done:
            try:
                still_configuring = []
                for d in self.droplets:
                    if not droplet_file_exists(d,'/.cloud-config-done'):
                        still_configuring.append(d)
                done = True
            except paramiko.ssh_exception.NoValidConnectionsError as error:
                ssh_errors += 1
                log.error('SSH failed... retrying %s/5',ssh_errors)
                time.sleep(2 * ssh_errors)
                ssh_error = error
                still_configuring = None
        if still_configuring is None:
            raise ssh_error
        return still_configuring

    @memoize
    def wait_for_build(self) -> None:
        log.info('Waiting for all droplets to finish building:')

        not_done = self.droplets_still_building()
        while not_done:
            log.info(' .. waiting on %s droplets', len(not_done))
            time.sleep(5)
            not_done = self.droplets_still_building()

        for function in self.postbuild_functions:
            if not self.dry_run:
                function()
            else:
                log.info(' .. DRY RUN .. did not run {}'.format(function))

        not_configured = self.droplets_still_configuring()
        while not_configured:
            log.info(' .. waiting for %s droplets to cloud-config',
                     len(not_configured))
            time.sleep(5)
            not_configured = self.droplets_still_configuring()
        close_ssh_clients()

        log.info('Done.')
        return True
