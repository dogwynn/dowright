from digitalocean.baseapi import BaseAPI, POST, DELETE
from digitalocean import Volume

class VolumeWithSnapshot(Volume):
    def __init__(self, *args, **kwargs):
        self.snapshot_id = None
        super().__init__(*args, **kwargs)
    def create_from_snapshot(self, *args, **kwargs):
        """
        Creates a Block Storage volume

        Note: Every argument and parameter given to this method will be
        assigned to the object.

        Args:
            name: string - a name for the volume
            snapshot_id: string - unique identifier for the volume snapshot
            size_gigabytes: int - size of the Block Storage volume in GiB

        Optional Args:
            description: string - text field to describe a volume
        """
        data = self.get_data('volumes/',
                             type=POST,
                             params={'name': self.name,
                                     'snapshot_id': self.snapshot_id,
                                     'size_gigabytes': self.size_gigabytes,
                                     'description': self.description})

        if data:
            self.id = data['volume']['id']
            self.created_at = data['volume']['created_at']

        return self


